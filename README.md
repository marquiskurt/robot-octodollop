# Octodollop

<img src = "app-icon-512x.png" width = "128px" alt="Octodollop logo" align = "left" style = "padding-right: 25px;"/>
_**A playground for robots**_

Otcodollop is the playground material used to create the Resonance software by the MechaMonarchs (2896). This code is licensed under the Apache 2.0 License; feel free to use this code to mess around with PyQt and RobotPy. If you want something more solid, check out the Resonance repository.

[![build status](https://www.gitlab.com/ci/projects/4091179/status.svg?ref=master)](https://www.gitlab.com/ci/projects/4091179?ref=master)

## Downloads
Currently, there are no downloads available yet, but there are plans to release a Snappy app for testing purposes. The snap can be installed by typing the following command onto any snap-ready system: `snap install robot-octodollop --edge --devmode`.

### If you are looking to download Resonance, check out the [official repository on GitLab](http://www.gitlab.com/damien-mechamonarchs/resonance).

##### _Created by Marquis Kurt with ❤️_
